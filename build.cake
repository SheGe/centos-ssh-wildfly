#addin nuget:?package=Cake.Docker&version=0.8.2
#addin nuget:?package=Cake.FileHelpers

// -----------------------------------------------------------------------------
// Arguments
// -----------------------------------------------------------------------------
var target = Argument("target", "Default");
var noCache = Argument("nocache", false);

var imageName = Argument("imageName", "shege/centos-ssh-wildfly");

var dockerRegistry = Argument("dockerRegistry", "registry.gitlab.com");
var dockerUser = Argument<string>("dockerUser", null);
var dockerPassword = Argument<string>("dockerPassword", null);

// -----------------------------------------------------------------------------
// Wildfly-Base
// -----------------------------------------------------------------------------
const string wildflyBaseImageName = "centos-ssh-wildfly-base";
string wildflyBaseVersion = FileReadText(".version");

Task("Wildfly-Base-Build")
    .Does(() =>
{
    var settings = new DockerImageBuildSettings
    {
        ForceRm = true,
        File = File("Dockerfile"),
        Tag = new []
        {
            $"{dockerRegistry}/{imageName}/{wildflyBaseImageName}:{wildflyBaseVersion}",
            $"{dockerRegistry}/{imageName}/{wildflyBaseImageName}:latest",
        },
        NoCache = noCache,
    };

    DockerBuild(settings, ".");
});

Task("Wildfly-Base-Publish")
    .IsDependentOn("Wildfly-Base-Build")
    .Does(() =>
{
    LoginToDockerRegistry(dockerRegistry);
    var versions = new []
    {
        $"{dockerRegistry}/{imageName}/centos-ssh-wildfly-base:{wildflyBaseVersion}",
        $"{dockerRegistry}/{imageName}/centos-ssh-wildfly-base:latest",
    };

    foreach (var version in versions)
    {
        DockerPush(version);
    }
});

// -----------------------------------------------------------------------------
// Wildfly-8
// -----------------------------------------------------------------------------
const string wildfly8ImageName = "centos-ssh-wildfly-8";
string wildfly8Version = FileReadText(Directory("Wildfly-8") + File(".version"));

Task("Wildfly-8-Build")
    .IsDependentOn("Wildfly-Base-Build")
    .Does(() =>
{
    var settings = new DockerImageBuildSettings
    {
        ForceRm = true,
        File = Directory("Wildfly-8") + File("Dockerfile"),
        Tag = new []
        {
            $"{dockerRegistry}/{imageName}/{wildfly8ImageName}:{wildfly8Version}",
            $"{dockerRegistry}/{imageName}/{wildfly8ImageName}:latest",
        },
        NoCache = noCache,
    };

    DockerBuild(settings, ".");
});

Task("Wildfly-8-Publish")
    .IsDependentOn("Wildfly-8-Build")
    .Does(() =>
{
    LoginToDockerRegistry(dockerRegistry);

    var versions = new []
    {
        $"{dockerRegistry}/{imageName}/{wildfly8ImageName}:{wildfly8Version}",
        $"{dockerRegistry}/{imageName}/{wildfly8ImageName}:latest",
    };

    foreach (var version in versions)
    {
        DockerPush(version);
    }
});

// -----------------------------------------------------------------------------
// Wildfly-10
// -----------------------------------------------------------------------------
const string wildfly10ImageName = "centos-ssh-wildfly-10";
string wildfly10Version = FileReadText(Directory("Wildfly-10") + File(".version"));

Task("Wildfly-10-Build")
    .IsDependentOn("Wildfly-Base-Build")
    .Does(() =>
{
    var settings = new DockerImageBuildSettings
    {
        ForceRm = true,
        File = Directory("Wildfly-10") + File("Dockerfile"),
        Tag = new []
        {
            $"{dockerRegistry}/{imageName}/{wildfly10ImageName}:{wildfly10Version}",
            $"{dockerRegistry}/{imageName}/{wildfly10ImageName}:latest",
        },
        NoCache = noCache,
    };

    DockerBuild(settings, ".");
});

Task("Wildfly-10-Publish")
    .IsDependentOn("Wildfly-10-Build")
    .Does(() =>
{
    LoginToDockerRegistry(dockerRegistry);

    var versions = new []
    {
        $"{dockerRegistry}/{imageName}/{wildfly10ImageName}:{wildfly10Version}",
        $"{dockerRegistry}/{imageName}/{wildfly10ImageName}:latest",
    };

    foreach (var version in versions)
    {
        DockerPush(version);
    }
});

// -----------------------------------------------------------------------------
// Helpers
// -----------------------------------------------------------------------------
private void LoginToDockerRegistry(string registryName = null)
{
    var parameters = "login -u {0} -p {1}";
    if (registryName == null)
    {
        // DockerLogin(dockerRegistry, publicDockerRegistryPassword);
        StartProcess("docker", string.Format(parameters, dockerUser, dockerPassword));
    }
    else
    {
        // DockerLogin(dockerRegistry, privateDockerRegistryPassword, registryName);
        StartProcess("docker", string.Format(parameters, dockerUser, dockerPassword) + " " + registryName);
    }
}

// -----------------------------------------------------------------------------
// General
// -----------------------------------------------------------------------------
Task("BuildAll")
    .IsDependentOn("Wildfly-8-Build")
    .IsDependentOn("Wildfly-10-Build");

Task("PublishAll")
    .IsDependentOn("Wildfly-Base-Publish")
    .IsDependentOn("Wildfly-8-Publish")
    .IsDependentOn("Wildfly-10-Publish");

Task("Default")
    .IsDependentOn("BuildAll");

RunTarget(target);