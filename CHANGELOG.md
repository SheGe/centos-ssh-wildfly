# CHANGELOG

## v1.0.4
* Fixed addition of krb5 security on each container start
* Load system environment variables before wildfly start
* The base image has been upgraded to [shege/centos-ssh-oraclejava:1.0.5](https://gitlab.com/SheGe/centos-ssh-oraclejava)

## v1.0.3

* The base image has been upgraded to [shege/centos-ssh-oraclejava:1.0.4](https://gitlab.com/SheGe/centos-ssh-oraclejava)

## v1.0.2

* Fixed wildfly management through supervisorctl

## v1.0.1

* The base image has been upgraded to [shege/centos-ssh-oraclejava:1.0.3](https://gitlab.com/SheGe/centos-ssh-oraclejava)
* Cake bootstraper files updated to support cake protocol change from [HTTP to HTTPS](https://cakebuild.net/blog/2017/08/http-to-https)

## v1.0.0

* Created the docker images with WILDFLY in specific versions installed
* The [shege/centos-ssh-oraclejava:1.0.1](https://gitlab.com/SheGe/centos-ssh-oraclejava) was choosen as base OS image (CentOS 7 with Oracle JAVA) to add SSH featureas and option to run multiple services in container by [supervisord](http://supervisord.org/).