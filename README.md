# CentOS (SSH) - Wildfly Containers

## Description

With Wildfly containers, you can simplify the configuration of Wildfly server to execution of one command only. The manual configuration is no longer required - everything within reach.

With Wildfly containers, you can:

* Deploy and configure your application through SSH/SFTP in CI/CD process
* Deploy an application by Wildfly management interface
* Configure server and container by environment variables
* and more...

## Tags and respective `Dockerfile` links

* Wildfly 10.1.0.Final - centos-ssh-wildfly-10, 1.0.4 ([Wildfly-10/Dockerfile](/Wildfly-10/Dockerfile))
* Wildfly 8.2.1.Final - centos-ssh-wildfly-8, 1.0.4 ([Wildfly-8/Dockerfile](/Wildfly-8/Dockerfile)])
* Wildfly Base - centos-ssh-wildfly-base, 1.0.4 [Dockerfile](./Dockerfile)

## Quick start

### Getting an images

The Wildfly docker images can be pulled from the `gitlab` docker registry by execution of commands below:

* Wildfly 8

```
docker pull registry.gitlab.com/shege/centos-ssh-wildfly/centos-ssh-wildfly-8
```

* Wildfly 10

```
docker pull registry.gitlab.com/shege/centos-ssh-wildfly/centos-ssh-wildfly-10
```

### Running the containers

Run the new container with randomly assigned ports:

```
docker run -d -p :22 -p :8080 -p :9990 registry.gitlab.com/shege/centos-ssh-wildfly/centos-ssh-wildfly-10
```

Check the assigned ports:

```
docker ps
```

```
... shege/centos-ssh-wildfly-10 ... Up 5 seconds (healthy)   0.0.0.0:32770->22/tcp, 0.0.0.0:32769->8080/tcp, 0.0.0.0:32768->9990/tcp ...
```

Open your favourite web browser and navigate to `localhost:32769` and voila (remember to change port to the assigned for 8080/tcp mapping).

## Environment variables

* SSH specific (*inherited from* [jdeathe/centos-ssh](https://store.docker.com/community/images/jdeathe/centos-ssh))

```
  SSH_AUTHORIZED_KEYS=""
  SSH_AUTOSTART_SSHD=true
  SSH_AUTOSTART_SSHD_BOOTSTRAP=true
  SSH_CHROOT_DIRECTORY="%h"
  SSH_INHERIT_ENVIRONMENT=false
  SSH_SUDO="ALL=(ALL) ALL"
  SSH_USER="app-admin"
  SSH_USER_FORCE_SFTP=false
  SSH_USER_HOME="/home/%u"
  SSH_USER_ID="500:500"
  SSH_USER_PASSWORD=""
  SSH_USER_PASSWORD_HASHED=false
  SSH_USER_SHELL="/bin/bash"
```

* Wildfly specific

```
  WILDFLY_AUTOSTART_STANDALONE=true
  WILDFLY_ADMIN_USER="admin"
  WILDFLY_ADMIN_PASSWORD="adminPassword"
  KRB5_LOGIN_MODULE_ENABLED=false
```

## Examples

* Running the new instance of Wildfly 10 in docker container with SSH account disabled:

```
docker run -d --name wildfly-10 -p 22:22 -p 8080:8080 -p 9990:9999 \
  --env "WILDFLY_ADMIN_USER=admin" \
  --env "WILDFLY_ADMIN_PASSWORD=password" \
  --env "SSH_AUTOSTART_SSHD=false" \
  registry.gitlab.com/shege/centos-ssh-wildfly/centos-ssh-wildfly-10
```

* Running the new instance of Wildfly 10 in docker container with SSH account enabled:

```
docker run -d --name wildfly-10 -p 22:22 -p 8080:8080 -p 9990:9999 \
  --env "WILDFLY_ADMIN_USER=admin" \
  --env "WILDFLY_ADMIN_PASSWORD=password" \
  --env "SSH_USER=devops" \
  --env "SSH_USER_PASSWORD=pass" \
  registry.gitlab.com/shege/centos-ssh-wildfly/centos-ssh-wildfly-10
```

* Running the new instance of Wildfly 10 in docker container with SSH account enabled - ssh user have the group required to manipulate `JBOSS_HOME` directory:

```
docker run -d --name wildfly-10 -p 22:22 -p 8080:8080 -p 9990:9999 \
  --env "WILDFLY_ADMIN_USER=admin" \
  --env "WILDFLY_ADMIN_PASSWORD=password" \
  --env "SSH_USER=devops" \
  --env "SSH_USER_PASSWORD=pass" \
  --env "SSH_USER_GROUPS="jboss,users,wheel"
  registry.gitlab.com/shege/centos-ssh-wildfly/centos-ssh-wildfly-10
```

## Agreements

The source code of these containers is on the [MIT License](./LICENSE), but usage of this container requires an agreement for [Oracle License](http://www.oracle.com/technetwork/java/javase/terms/license/index.html), aspecially:

> Use of the Commercial Features for any commercial or production purpose requires a separate license from Oracle. "Commercial Features" means those features identified Table 1-1 (Commercial Features In Java SE Product Editions) of the Java SE documentation accessible at http://www.oracle.com/technetwork/java/javase/documentation/index.html
