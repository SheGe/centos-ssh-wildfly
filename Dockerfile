FROM registry.gitlab.com/shege/centos-ssh-oraclejava:1.0.5

EXPOSE 22 8080 9990

ENV JBOSS_HOME="/opt/jboss/wildfly" \
    LAUNCH_JBOSS_IN_BACKGROUND=true \
    WILDFLY_AUTOSTART_STANDALONE=true \
    WILDFLY_ADMIN_USER="admin" \
    WILDFLY_ADMIN_PASSWORD="adminPassword" \
    KRB5_LOGIN_MODULE_ENABLED=false

# -----------------------------------------------------------------------------
# Wildfly - directory owner
# -----------------------------------------------------------------------------
# Create a user and group used to launch processes
# The user ID 1000 is the default for the first "regular" user on Fedora/RHEL,
# so there is a high chance that this ID will be equal to the current user
# making it easier to use volumes (no permission issues)
# -----------------------------------------------------------------------------
RUN groupadd -r jboss -g 1000 && useradd -u 1000 -r -g jboss -m -d /opt/jboss -s /sbin/nologin -c "JBoss user" jboss && \
    chmod 755 /opt/jboss

# -----------------------------------------------------------------------------
# Copy files into place
# -----------------------------------------------------------------------------
ADD src/usr/sbin \
        /usr/sbin/

ADD src/etc/services-config/supervisor/supervisord.d/wildfly-standalone.conf \
        /etc/services-config/supervisor/supervisord.d/wildfly-standalone.conf

ADD src/etc/services-config/supervisor/supervisord.d/wildfly-bootstrap.conf \
        /etc/services-config/supervisor/supervisord.d/wildfly-bootstrap.conf

RUN ln -sf \
        /etc/services-config/supervisor/supervisord.d/wildfly-standalone.conf \
        /etc/supervisord.d/wildfly-standalone.conf \
    && ln -sf \
        /etc/services-config/supervisor/supervisord.d/wildfly-bootstrap.conf \
        /etc/supervisord.d/wildfly-bootstrap.conf \
    && chmod 700 \
        /usr/sbin/wildfly-bootstrap

ADD .version centos-ssh-wildfly-base.version